﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T item)
        {
            (Data as ICollection<T>).Add(item);
            return Task.CompletedTask;
        }

        public Task<Employee> UpdateEmployeeAsync(Employee item)
        {
            Employee itemFromRepo = (Data as ICollection<Employee>).FirstOrDefault(i => i.Id == item.Id);

            itemFromRepo.FirstName = item.FirstName;
            itemFromRepo.LastName = item.LastName;
            itemFromRepo.Email = item.Email;
            itemFromRepo.AppliedPromocodesCount = item.AppliedPromocodesCount;

            if (item.Roles != null && item.Roles.Count > 0)
            {
                itemFromRepo.Roles.Clear();

                foreach (Role role in item.Roles)
                {
                    itemFromRepo.Roles.Add(role);
                }
            }

            return Task.FromResult(itemFromRepo);
        }

        public Task DeleteAsync(Guid id)
        {
            var item = (Data as ICollection<T>).FirstOrDefault(i => i.Id == id);
            (Data as List<T>).Remove(item);
            return Task.CompletedTask;
        }
    }
}